#!/bin/bash

while true; do
    echo "a - Executar ping para um host"
    echo "b - Listar usuários logados"
    echo "c - Exibir uso de memória e disco"
    echo "d - Sair"
    read -p "Escolha uma opção: " escolha
    echo

    case "$escolha" in
        a)
            read -p "Digite o host (IP ou site): " host
            ping -c 5 "$host"
            ;;
        b)
            who
            ;;
        c)
            df -h
            free -h
            ;;
        d)
            echo "Saindo..."
            exit 0
            ;;
        *)
            echo "Opção inválida. Por favor, escolha uma opção válida."
            ;;
    esac

    echo
done
